package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowerConstants;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static com.epam.rd.java.basic.task8.entity.Flower.GrowingTips;
import static com.epam.rd.java.basic.task8.entity.Flower.GrowingTips.*;
import static com.epam.rd.java.basic.task8.entity.Flower.VisualParameters;
import static com.epam.rd.java.basic.task8.entity.Flower.VisualParameters.AveLenFlower;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    Flowers parseDocument() {
        Flowers flowers = new Flowers();
        Flower flower = null;
        VisualParameters visualParameters = null;
        GrowingTips growingTips = null;
        AveLenFlower aveLenFlower = null;
        Tempreture tempreture = null;
        Lighting lighting = null;
        Watering watering = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();

                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();

                    if (startElement.getName().getLocalPart().equals(FlowerConstants.FLOWER)) {
                        flower = new Flower();
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.NAME)) {
                        xmlEvent = xmlEventReader.nextEvent();
                        flower.setName(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.SOIL)) {
                        xmlEvent = xmlEventReader.nextEvent();
                        flower.setSoil(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.ORIGIN)) {
                        xmlEvent = xmlEventReader.nextEvent();
                        flower.setOrigin(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.VISUAL_PARAMETERS)) {
                        visualParameters = new VisualParameters();
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.STEM_COLOUR)) {
                        xmlEvent = xmlEventReader.nextEvent();
                        visualParameters.setStemColour(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.LEAF_COLOUR)) {
                        xmlEvent = xmlEventReader.nextEvent();
                        visualParameters.setLeafColour(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.AVE_LEN_FLOWER)) {
                        aveLenFlower = new AveLenFlower();

                        Attribute measureAttr = startElement.getAttributeByName(new QName(FlowerConstants.MEASURE));
                        if (measureAttr != null) {
                            aveLenFlower.setMeasure(measureAttr.getValue());
                        }

                        xmlEvent = xmlEventReader.nextEvent();
                        aveLenFlower.setValue(Long.parseLong(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.GROWING_TIPS)) {
                        growingTips = new GrowingTips();
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.TEMPRETURE)) {
                        tempreture = new Tempreture();

                        Attribute measureAttr = startElement.getAttributeByName(new QName(FlowerConstants.MEASURE));
                        if (measureAttr != null) {
                            tempreture.setMeasure(measureAttr.getValue());
                        }

                        xmlEvent = xmlEventReader.nextEvent();
                        tempreture.setValue(Long.parseLong(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.LIGHTING)) {
                        lighting = new Lighting();

                        Attribute lightingRequirement = startElement.getAttributeByName(new QName(FlowerConstants.LIGHT_REQUIREMENTS));
                        if (lightingRequirement != null) {
                            lighting.setLightRequiring(lightingRequirement.getValue());
                        }
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.WATERING)) {
                        watering = new Watering();

                        Attribute measureAttr = startElement.getAttributeByName(new QName(FlowerConstants.MEASURE));
                        if (measureAttr != null) {
                            watering.setMeasure(measureAttr.getValue());
                        }

                        xmlEvent = xmlEventReader.nextEvent();
                        watering.setValue(Long.parseLong(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(FlowerConstants.MULTIPLYING)) {
						xmlEvent = xmlEventReader.nextEvent();
						flower.setMultiplying(xmlEvent.asCharacters().getData());
					}
				}

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals(FlowerConstants.FLOWER)) {
                        flowers.addFlower(flower);
                    } else if (endElement.getName().getLocalPart().equals(FlowerConstants.VISUAL_PARAMETERS)) {
                        flower.setVisualParameters(visualParameters);
                    } else if (endElement.getName().getLocalPart().equals(FlowerConstants.AVE_LEN_FLOWER)) {
                        visualParameters.setAveLenFlower(aveLenFlower);
                    } else if (endElement.getName().getLocalPart().equals(FlowerConstants.GROWING_TIPS)) {
                        flower.setGrowingTips(growingTips);
                    } else if (endElement.getName().getLocalPart().equals(FlowerConstants.TEMPRETURE)) {
                        growingTips.setTempreture(tempreture);
                    } else if (endElement.getName().getLocalPart().equals(FlowerConstants.LIGHTING)) {
                        growingTips.setLighting(lighting);
                    } else if (endElement.getName().getLocalPart().equals(FlowerConstants.WATERING)) {
                        growingTips.setWatering(watering);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }

		flowers.getFlowers().forEach(System.out::println);
        return flowers;
    }
}