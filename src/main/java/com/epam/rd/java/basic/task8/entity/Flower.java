
package com.epam.rd.java.basic.task8.entity;


public class Flower {
    protected String name;
    protected String soil;
    protected String origin;
    protected VisualParameters visualParameters;
    protected GrowingTips growingTips;
    protected String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String value) {
        this.soil = value;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String value) {
        this.origin = value;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters value) {
        this.visualParameters = value;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips value) {
        this.growingTips = value;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String value) {
        this.multiplying = value;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }

    public static class GrowingTips {
        protected Tempreture tempreture;
        protected Lighting lighting;
        protected Watering watering;

        public Tempreture getTempreture() {
            return tempreture;
        }

        public void setTempreture(Tempreture value) {
            this.tempreture = value;
        }

        public Lighting getLighting() {
            return lighting;
        }

        public void setLighting(Lighting value) {
            this.lighting = value;
        }

        public Watering getWatering() {
            return watering;
        }

        public void setWatering(Watering value) {
            this.watering = value;
        }

        @Override
        public String toString() {
            return "GrowingTips{" +
                    "tempreture=" + tempreture +
                    ", lighting=" + lighting +
                    ", watering=" + watering +
                    '}';
        }

        public static class Lighting {
            protected String lightRequiring;

            public String getLightRequiring() {
                return lightRequiring;
            }

            public void setLightRequiring(String value) {
                this.lightRequiring = value;
            }

            @Override
            public String toString() {
                return "Lighting{" +
                        "lightRequiring='" + lightRequiring + '\'' +
                        '}';
            }
        }

        public static class Tempreture {

            protected Long value;
            protected String measure = "celcius";

            public Long getValue() {
                return value;
            }

            public void setValue(Long value) {
                this.value = value;
            }

            public String getMeasure() {
                return measure;
            }

            public void setMeasure(String value) {
                this.measure = value;
            }

            @Override
            public String toString() {
                return "Tempreture{" +
                        "value=" + value +
                        ", measure='" + measure + '\'' +
                        '}';
            }
        }


        public static class Watering {

            protected Long value;
            protected String measure = "mlPerWeek";

            public Long getValue() {
                return value;
            }

            public void setValue(Long value) {
                this.value = value;
            }

            public String getMeasure() {
                return measure;
            }

            public void setMeasure(String value) {
                this.measure = value;
            }

            @Override
            public String toString() {
                return "Watering{" +
                        "value=" + value +
                        ", measure='" + measure + '\'' +
                        '}';
            }
        }

    }

    public static class VisualParameters {
        protected String stemColour;
        protected String leafColour;
        protected AveLenFlower aveLenFlower;

        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String value) {
            this.stemColour = value;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String value) {
            this.leafColour = value;
        }

        public AveLenFlower getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(AveLenFlower value) {
            this.aveLenFlower = value;
        }

        @Override
        public String toString() {
            return "VisualParameters{" +
                    "stemColour='" + stemColour + '\'' +
                    ", leafColour='" + leafColour + '\'' +
                    ", aveLenFlower=" + aveLenFlower +
                    '}';
        }

        public static class AveLenFlower {
            protected Long value;
            protected String measure = "cm";

            public Long getValue() {
                return value;
            }

            public void setValue(Long value) {
                this.value = value;
            }

            public String getMeasure() {
                return measure;
            }

            public void setMeasure(String value) {
                this.measure = value;
            }

            @Override
            public String toString() {
                return "AveLenFlower{" +
                        "value=" + value +
                        ", measure='" + measure + '\'' +
                        '}';
            }
        }
    }
}

