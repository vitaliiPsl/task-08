package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips.Lighting;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips.Tempreture;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips.Watering;
import com.epam.rd.java.basic.task8.entity.Flower.VisualParameters;
import com.epam.rd.java.basic.task8.entity.Flower.VisualParameters.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.FlowerConstants;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public Flowers parseDocument(){
		Flowers flowers = null;

		try {
			File inputFile = new File(xmlFileName);
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			FlowersHandler handler = new FlowersHandler();

			saxParser.parse(inputFile, handler);
			flowers = handler.getResult();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flowers;
	}
}

class FlowersHandler extends DefaultHandler{
	private StringBuilder currentValue = new StringBuilder();
	private Flowers flowers;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;
	private AveLenFlower aveLenFlower;
	private Watering watering;
	private Tempreture tempreture;
	private Lighting lighting;

	public Flowers getResult() {
		flowers.getFlowers().forEach(System.out::println);
		return flowers;
	}

	@Override
	public void startDocument() {
		flowers = new Flowers();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {

		// reset the tag value
		currentValue.setLength(0);

		// start of loop
		if (qName.equalsIgnoreCase(FlowerConstants.FLOWER)) {
			// new flower
			flower = new Flower();
		}

		if (qName.equalsIgnoreCase(FlowerConstants.VISUAL_PARAMETERS)) {
			visualParameters = new VisualParameters();
		}

		if (qName.equalsIgnoreCase(FlowerConstants.GROWING_TIPS)) {
			growingTips = new GrowingTips();
		}

		if (qName.equalsIgnoreCase(FlowerConstants.AVE_LEN_FLOWER)) {
			aveLenFlower = new AveLenFlower();

			String measure = attributes.getValue(FlowerConstants.MEASURE);
			aveLenFlower.setMeasure(measure);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.TEMPRETURE)) {
			tempreture = new Tempreture();

			String measure = attributes.getValue(FlowerConstants.MEASURE);
			tempreture.setMeasure(measure);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.LIGHTING)) {
			lighting = new Lighting();

			String lightRequiring = attributes.getValue(FlowerConstants.LIGHT_REQUIREMENTS);
			lighting.setLightRequiring(lightRequiring);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.WATERING)) {
			watering = new Watering();

			String measure = attributes.getValue(FlowerConstants.MEASURE);
			watering.setMeasure(measure);
		}

	}

	public void endElement(String uri, String localName, String qName) {

		if (qName.equalsIgnoreCase(FlowerConstants.NAME)) {
			flower.setName(currentValue.toString());
		}

		if (qName.equalsIgnoreCase(FlowerConstants.SOIL)) {
			flower.setSoil(currentValue.toString());
		}

		if (qName.equalsIgnoreCase(FlowerConstants.ORIGIN)) {
			flower.setOrigin(currentValue.toString());
		}

		if (qName.equalsIgnoreCase(FlowerConstants.STEM_COLOUR)) {
			visualParameters.setStemColour(currentValue.toString());
		}

		if (qName.equalsIgnoreCase(FlowerConstants.LEAF_COLOUR)) {
			visualParameters.setLeafColour(currentValue.toString());
		}

		if (qName.equalsIgnoreCase(FlowerConstants.AVE_LEN_FLOWER)) {
			aveLenFlower.setValue(Long.parseLong(currentValue.toString()));
			visualParameters.setAveLenFlower(aveLenFlower);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.VISUAL_PARAMETERS)) {
			flower.setVisualParameters(visualParameters);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.TEMPRETURE)) {
			tempreture.setValue(Long.parseLong(currentValue.toString()));
			growingTips.setTempreture(tempreture);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.LIGHTING)) {
			growingTips.setLighting(lighting);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.WATERING)) {
			watering.setValue(Long.parseLong(currentValue.toString()));
			growingTips.setWatering(watering);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.GROWING_TIPS)) {
			flower.setGrowingTips(growingTips);
		}

		if (qName.equalsIgnoreCase(FlowerConstants.MULTIPLYING)) {
			flower.setMultiplying(currentValue.toString());
		}

		// end of loop
		if (qName.equalsIgnoreCase(FlowerConstants.FLOWER)) {
			flowers.addFlower(flower);
		}

	}

	public void characters(char ch[], int start, int length) {
		currentValue.append(ch, start, length);

	}
}