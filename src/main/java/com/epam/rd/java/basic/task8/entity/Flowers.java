package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Flowers {
    List<Flower> flowers;

    public Flowers(){
        flowers = new ArrayList<>();
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public void addFlower(Flower flower){
        flowers.add(flower);
    }

    public static Comparator<Flower> getNameComparator(){
        return Comparator.comparing(Flower::getName);
    }

    public static Comparator<Flower> getWateringComparator(){
        return (f1, f2) -> {
            long w1 = f1.getGrowingTips().getWatering().getValue();
            long w2 = f2.getGrowingTips().getWatering().getValue();
            return (int) (w1 - w2);
        };
    }

    public static Comparator<Flower> getGrowingTemperature(){
        return (f1, f2) -> {
            long t1 = f1.getGrowingTips().getTempreture().getValue();
            long t2 = f2.getGrowingTips().getTempreture().getValue();
            return (int) (t1 - t2);
        };
    }

}
