package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips.Lighting;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips.Tempreture;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips.Watering;
import com.epam.rd.java.basic.task8.entity.Flower.VisualParameters;
import com.epam.rd.java.basic.task8.entity.Flower.VisualParameters.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.FlowerConstants;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public Flowers parseDocument() {
		Flowers flowers = null;

		// Instantiate the Factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

			DocumentBuilder db = dbf.newDocumentBuilder();

			File file = new File(xmlFileName);
			Document doc = db.parse(file);
			NodeList list = doc.getElementsByTagName(FlowerConstants.FLOWER);

			flowers = parseFlowers(list);

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		return flowers;
	}

	public void saveXMLData(Flowers flowers, String fileName){
		try{
			DocumentBuilderFactory dbFactory =
					DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();

			writeFlowers(flowers, doc);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileName));
			transformer.transform(source, result);

		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
	}

	private void writeFlowers(Flowers flowers, Document doc) {
		// write <flowers>
		Element root = doc.createElement(FlowerConstants.FLOWERS);
//		root.setAttributeNS("http://www.w3.org/2001/XMLSchema-instance",
//				"xsi:schemaLocation", "http://www.nure.ua input.xsd");
		root.setAttribute("xmlns", "http://www.nure.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
		doc.appendChild(root);

		for(Flower flower : flowers.getFlowers()){
			// write <flower>
			Element flowerElement = doc.createElement(FlowerConstants.FLOWER);
			root.appendChild(flowerElement);

			// write <name>
			Element flowerName = doc.createElement(FlowerConstants.NAME);
			flowerName.appendChild(doc.createTextNode(flower.getName()));
			flowerElement.appendChild(flowerName);

			// write soil
			Element flowerSoil = doc.createElement(FlowerConstants.SOIL);
			flowerSoil.appendChild(doc.createTextNode(flower.getSoil()));
			flowerElement.appendChild(flowerSoil);

			// write origin
			Element flowerOrigin = doc.createElement(FlowerConstants.ORIGIN);
			flowerOrigin.appendChild(doc.createTextNode(flower.getOrigin()));
			flowerElement.appendChild(flowerOrigin);

			// write <visualParameters>
			VisualParameters visualParameters = flower.getVisualParameters();

			Element visualParametersElement = doc.createElement(FlowerConstants.VISUAL_PARAMETERS);

			Element stemColour = doc.createElement(FlowerConstants.STEM_COLOUR);
			stemColour.appendChild(doc.createTextNode(visualParameters.getStemColour()));
			visualParametersElement.appendChild(stemColour);

			Element leafColour = doc.createElement(FlowerConstants.LEAF_COLOUR);
			leafColour.appendChild(doc.createTextNode(visualParameters.getLeafColour()));
			visualParametersElement.appendChild(leafColour);

			// write <aveLenFlower>
			Element aveLenFlower = doc.createElement(FlowerConstants.AVE_LEN_FLOWER);
			Attr aveLenFlowerMeasure = doc.createAttribute(FlowerConstants.MEASURE);
			aveLenFlowerMeasure.setValue(visualParameters.getAveLenFlower().getMeasure());
			aveLenFlower.setAttributeNode(aveLenFlowerMeasure);
			aveLenFlower.appendChild(doc.createTextNode(visualParameters.getAveLenFlower().getValue().toString()));
			visualParametersElement.appendChild(aveLenFlower);
			flowerElement.appendChild(visualParametersElement);

			// write <growingTips>
			GrowingTips growingTips = flower.getGrowingTips();
			Element growingTipsElement = doc.createElement(FlowerConstants.GROWING_TIPS);

			// write <tempreture>
			Element tempreture = doc.createElement(FlowerConstants.TEMPRETURE);
			Attr tempretureMeasure = doc.createAttribute(FlowerConstants.MEASURE);
			tempretureMeasure.setValue(growingTips.getTempreture().getMeasure());
			tempreture.setAttributeNode(tempretureMeasure);
			tempreture.appendChild(doc.createTextNode(growingTips.getTempreture().getValue().toString()));
			growingTipsElement.appendChild(tempreture);

			// write <lighting>
			Element lighting = doc.createElement(FlowerConstants.LIGHTING);
			Attr lightingRequirement = doc.createAttribute(FlowerConstants.LIGHT_REQUIREMENTS);
			lightingRequirement.setValue(growingTips.getLighting().getLightRequiring());
			lighting.setAttributeNode(lightingRequirement);
			growingTipsElement.appendChild(lighting);

			// write <watering>
			Element watering = doc.createElement(FlowerConstants.WATERING);
			Attr wateringMeasure = doc.createAttribute(FlowerConstants.MEASURE);
			wateringMeasure.setValue(growingTips.getWatering().getMeasure());
			watering.setAttributeNode(wateringMeasure);
			watering.appendChild(doc.createTextNode(growingTips.getWatering().getValue().toString()));
			growingTipsElement.appendChild(watering);

			flowerElement.appendChild(growingTipsElement);

			// write <multiplying>

			Element multiplying = doc.createElement(FlowerConstants.MULTIPLYING);
			multiplying.appendChild(doc.createTextNode(flower.getMultiplying()));
			flowerElement.appendChild(multiplying);
		}
	}

	private Flowers parseFlowers(NodeList list) {
		Flowers flowers = new Flowers();

		for (int i = 0; i < list.getLength(); i++) {
			Node node = list.item(i);
			Flower flower;

			flower = parseFlower(node);

			flowers.addFlower(flower);
		}

		return flowers;
	}

	private Flower parseFlower(Node node) {
		Flower flower = null;

		if (node.getNodeType() == Node.ELEMENT_NODE) {
			flower = new Flower();

			// get <flower>
			Element element = (Element) node;

			// get text
			String name = element.getElementsByTagName(FlowerConstants.NAME).item(0).getTextContent();
			String soil = element.getElementsByTagName(FlowerConstants.SOIL).item(0).getTextContent();
			String origin = element.getElementsByTagName(FlowerConstants.ORIGIN).item(0).getTextContent();

			flower.setName(name);
			flower.setSoil(soil);
			flower.setOrigin(origin);

			VisualParameters visualParameters = getVisualParameters(element);
			flower.setVisualParameters(visualParameters);

			GrowingTips growingTips = getGrowingTips(element);
			flower.setGrowingTips(growingTips);

			String multiplying = element.getElementsByTagName(FlowerConstants.MULTIPLYING).item(0).getTextContent();
			flower.setMultiplying(multiplying);
		}

		return flower;
	}

	private GrowingTips getGrowingTips(Element element) {
		//parse <growingTips>
		Element growingTipsElement = (Element) element.getElementsByTagName(FlowerConstants.GROWING_TIPS)
				.item(0);
		GrowingTips growingTips = new GrowingTips();

		Tempreture tempreture = getTempreture(growingTipsElement);
		growingTips.setTempreture(tempreture);

		Lighting lighting = getLighting(growingTipsElement);
		growingTips.setLighting(lighting);

		Watering watering = getWatering(growingTipsElement);
		growingTips.setWatering(watering);
		return growingTips;
	}

	private VisualParameters getVisualParameters(Element element) {
		// parse <visualParameters>
		Element visualParametersElement = (Element) element.getElementsByTagName(FlowerConstants.VISUAL_PARAMETERS)
				.item(0);

		VisualParameters visualParameters = new VisualParameters();

		String stemColour = visualParametersElement.getElementsByTagName(FlowerConstants.STEM_COLOUR).item(0).getTextContent();
		String leafColour = visualParametersElement.getElementsByTagName(FlowerConstants.LEAF_COLOUR).item(0).getTextContent();
		visualParameters.setStemColour(stemColour);
		visualParameters.setLeafColour(leafColour);

		AveLenFlower aveLenFlower = getAveLenFlower(visualParametersElement);

		visualParameters.setAveLenFlower(aveLenFlower);
		return visualParameters;
	}

	private AveLenFlower getAveLenFlower(Element visualParametersElement) {
		// parse <aveLenFlower>
		AveLenFlower aveLenFlower = new AveLenFlower();
		Node visualParametersNode = visualParametersElement.getElementsByTagName(FlowerConstants.AVE_LEN_FLOWER).item(0);
		long aveLenFlowerValue = Long.parseLong(visualParametersNode.getTextContent());
		String aveLenFlowerMeasure = visualParametersNode
				.getAttributes()
				.getNamedItem(FlowerConstants.MEASURE)
				.getTextContent();

		aveLenFlower.setValue(aveLenFlowerValue);
		aveLenFlower.setMeasure(aveLenFlowerMeasure);
		return aveLenFlower;
	}

	private Watering getWatering(Element growingTipsElement) {
		//parse <watering>
		Watering watering = new Watering();
		Node wateringNode = growingTipsElement.getElementsByTagName(FlowerConstants.WATERING).item(0);
		long wateringValue = Long.parseLong(wateringNode.getTextContent());
		String wateringMeasure = wateringNode
				.getAttributes()
				.getNamedItem(FlowerConstants.MEASURE)
				.getTextContent();

		watering.setValue(wateringValue);
		watering.setMeasure(wateringMeasure);
		return watering;
	}

	private Lighting getLighting(Element growingTipsElement) {
		//parse <lighting>
		Lighting lighting = new Lighting();
		Node lightingNode = growingTipsElement.getElementsByTagName(FlowerConstants.LIGHTING).item(0);
		String lightingRequirement = lightingNode
				.getAttributes()
				.getNamedItem(FlowerConstants.LIGHT_REQUIREMENTS)
				.getTextContent();

		lighting.setLightRequiring(lightingRequirement);
		return lighting;
	}

	private Tempreture getTempreture(Element growingTipsElement) {
		// parse <tempreture>
		Tempreture tempreture = new Tempreture();
		Node tempretureNode = growingTipsElement.getElementsByTagName(FlowerConstants.TEMPRETURE).item(0);
		long tempretureValue = Long.parseLong(tempretureNode.getTextContent());
		String tempretureMeasure = tempretureNode
				.getAttributes()
				.getNamedItem(FlowerConstants.MEASURE)
				.getTextContent();

		tempreture.setValue(tempretureValue);
		tempreture.setMeasure(tempretureMeasure);
		return tempreture;
	}
}
